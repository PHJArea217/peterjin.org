2018-12-04|4|-|-|IPv6 only Virtual Private Servers

We've long run out of IPv4 addresses. The creators of the Internet never thought
that 32 bits would be too small of an address. Yet now we are, in 2018, trading
IPv4 addresses like crazy and using all sorts of optimizations like network
address translation \("NAT") to squeeze out all that previously-wasted IPv4
space. In fact, I imagine that IPv4 addresses are increasing in price: supply
is inelastic \(quantity is constant at any price), but demand is constantly
increasing. Therefore, by the law of supply and demand, IPv4 addresses are
become more expensive.

But fortunately, we've got IPv6. It has a much longer 128-bit address of 32 hex
digits. It doesn't need NAT \(although there are legitimate uses for it such as
for simplifying routing tables or ensuring address stability), meaning that on
most networks, every device and computer in even a large private network can
have its own public IPv6 address. IPv4, on the other hand, is so exhausted that
you can pretty much only have one IPv4 address per Internet connection or
organization.

## The case for IPv6 deployment

Although my phone's cellular internet and the internet at home have both IPv4
and IPv6, IPv6 deployment in many other areas is virtually nonexistent. I have
been on public WiFi in many places -- Canada, China, even here at UIUC, and one
thing I've noticed was that there was not a single WiFi hotspot in any of the
places I've visited that offered IPv6 connectivity. Not even the UIUC public
computers had IPv6 -- Siebel, Grainger, UIUC WiFi, not even any illinois.edu
website \[n1]! In my opinion, it's such a pity that there are not enough places
and websites in the world with IPv6 connectivity, whether native or tunnelled
\(through a tunnel broker like Hurricane Electric or Sixxs), even though we've
already run out of IPv4 addresses. It may be expensive, but it'll be worth it
in the long run.

I imagine that there are many opportunities for IPv6 deployment that have been
missed because people only bother to include IPv4 addresses in the DNS and
completely forget IPv6 addresses, even when the Internet Service Provider
provides IPv6 addresses for free \(which is usually true). 

## My VPS

My main Virtual Private Server \(VPS), on the domain
[apps-vm2.peterjin.org](https://apps-vm2.peterjin.org), has both an IPv4 and
IPv6 address. \(There used to be an apps-vm1, but it was an old Google Cloud
Platform VM and I wanted to migrate to DigitalOcean instead.)
But IPv6 only VPS's also exist. Vultr, for example, offers a $2.50 per month
VPS with only an IPv6 address, but essentially, they can be reached from IPv6
but not IPv4. What this means is that only places with IPv6 connectivity \(very
few in my case, like my home internet or my cellular internet) can actually
reach this site, and places with IPv4 only connectivity, like most public WiFi
hotspots, cannot access this site. I know of at least one site,
[Loops of Zen](https://loopsofzen.uk), where this was true.

There are three main connections essential for any VPS used to host a website.
One is port 22 \(SSH), one is port 443, and one is port 80. Each needs to be
accessed over IPv6 for an IPv6-only VPS. This may be the end-all for servers
that only need IPv6 connectivity, but since many places don't have IPv6, it is
often not sufficient for many users who need the VPS accessible over IPv4.

## So how do we connect to it over IPv4?

Fortunately, we have this thing called "Cloudflare." It essentially acts as a
reverse proxy, and it can give IPv4 addresses to IPv6-only web sites and vice
versa. It's free for small personal sites. Simply sign up for an account,
add your domain, and change your domain's DNS servers at your registar to
Cloudflare's. Then, add your VPS's IPv6 address into Cloudflare DNS as an AAAA
record. By clicking on the grey cloud on an AAAA record, turning it into an
orange one, IPv4 web traffic will be routed through Cloudflare, which has both
IPv4 and IPv6 connectivity, and Cloudflare will make an IPv6 connection to the
origin server \(the VPS). \[n2]

Note that I said "web sites" and "web traffic." Only port 443 and port 80 will
be tunnelled. What's left is port 22, the SSH server. But there's an easy
solution for that too. Have another dual-stack VPS with both IPv4 and IPv6
somewhere on the Internet. You'll need only one, no matter how many IPv6 VPS's
you have \(and you can use an existing one if you'd like). What you'll do is
you'll SSH from your IPv4 connection to the dual-stack VPS through IPv4, and
then SSH from that dual-stack to the IPv6-only VPS through IPv6. If you're like
me and use only public keys instead of passwords for authentication, SSH agent
forwarding might be useful, as it is a typical case where you need an
intermediate proxy server to access another SSH server, but you don't want the
keys to be present on the intermediate.

There are a few leftover protocols like FTP and SMTP, but since most people
would use a VPS for website hosting, those are not strictly necessary, and for
those who do need it, it would probably be best to just put those on the
dual-stack VPS mentioned earlier.

Anyway, I feel like this presents a good use case for an IPv6-only VPS for
hosting a website, and I really hope that more and more places around the world
will have IPv6 connectivity. But until then, it'll probably be a while until
we see IPv6 wherever you go on public WiFi.

## Appendix A: This website's IPv6 connectivity

Thanks to Cloudflare, this domain, peterjin.org, and almost all subdomains --
www, webclock, busroutes, appsvr, apps-vm2, apps-vm2-cdn, go -- support IPv6.
Among other things, it also has DNSSEC and full HTTPS/SSL on all of the above
domains. The apex, www, and apps-vm2 subdomains score 100% on the Internet.nl
website test and A+ on Qualys' SSL test.

## Footnotes

\[n1] At least one of the DNS servers had IPv6, and CS 438 goes briefly into
IPv6...

\[n2] You'll want to set your SSL mode to "Full \(strict)" and install a
Cloudflare Origin CA certificate on your VPS. This is not strictly necessary,
but it would not be wise to use clear-text HTTP with no encryption between
Cloudflare and your VPS given that the web traffic can be easily seen by someone
who can see that connection.
